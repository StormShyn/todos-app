### Setups 🍰

```
1. Components
2. Json Server
3. Redux
4. Axios
```

### Packages 🍣

```
1. Nanoid
```

### Using 🍨

```
Type "npm run server" to start the server (db.json)
```
