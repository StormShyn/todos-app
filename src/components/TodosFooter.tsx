import { useState } from 'react';
import { useSelector } from 'react-redux';

// types
import { IRootState } from '../app/reducers';
import { ITodosState } from '../app/actionTypes';

import useActionCreators from '../hooks/useActionCreators';

function TodosFooter() {
  const [labelActive, setLabelActive] = useState<string>('all');

  const todos: ITodosState = useSelector((state: IRootState) => state.todos);
  const leftTodos = todos.filter((todo) => !todo.completed);

  const { getTodos, filterTodos, deleteTodo } = useActionCreators();

  const handleClearCompletedTodos = () => {
    todos.map((todo) => todo.completed && deleteTodo(todo.id));
  };

  return (
    <>
      {todos.length > 0 && (
        <footer className='footer'>
          <span className='todo-count'>
            <strong>{leftTodos.length}</strong> item left
          </span>

          <ul className='filters'>
            <li
              onClick={() => {
                setLabelActive('all');
                getTodos();
              }}>
              <a
                className={`${labelActive === 'all' ? 'selected' : ''}`}
                href='#/'>
                All
              </a>
            </li>
            <li
              onClick={() => {
                setLabelActive('active');
                filterTodos('active');
              }}>
              <a
                className={labelActive === 'active' ? 'selected' : ''}
                href='#/'>
                Active
              </a>
            </li>
            <li
              onClick={() => {
                setLabelActive('completed');
                filterTodos('completed');
              }}>
              <a
                className={labelActive === 'completed' ? 'selected' : ''}
                href='#/'>
                Completed
              </a>
            </li>
          </ul>

          {todos.some((todo) => todo.completed) && (
            <button
              onClick={() => handleClearCompletedTodos()}
              className='clear-completed'>
              Clear completed
            </button>
          )}
        </footer>
      )}
    </>
  );
}

export default TodosFooter;
