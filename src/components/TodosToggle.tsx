import { useSelector } from 'react-redux';

import useActionCreators from '../hooks/useActionCreators';

// types
import { ITodosState } from '../app/actionTypes';
import { IRootState } from '../app/reducers';

function TodosToggle() {
  const todos: ITodosState = useSelector((state: IRootState) => state.todos);

  const { toggleAllTodos } = useActionCreators();

  const toggleCheckedAll = () => {
    const isCompletedTodos: boolean = todos.every((todo) => todo.completed);

    todos.forEach((todo): void => {
      toggleAllTodos({
        ...todo,
        completed: isCompletedTodos ? false : true,
      });
    });
  };

  return (
    <>
      <input id='toggle-all' className='toggle-all' type='checkbox' />
      <label onClick={toggleCheckedAll} htmlFor='toggle-all'>
        Mark all as complete
      </label>
    </>
  );
}

export default TodosToggle;
