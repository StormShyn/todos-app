import { useEffect } from 'react';
import { useSelector } from 'react-redux';

// types
import { IRootState } from '../app/reducers';
import { ITodo, ITodosState } from '../app/actionTypes';

import useActionCreators from '../hooks/useActionCreators';

import TodosItem from './TodosItem';

function TodosList() {
  const todos: ITodosState = useSelector((state: IRootState) => state.todos);
  const reversedTodos = [...todos].reverse();

  const { getTodos } = useActionCreators();

  useEffect(() => {
    getTodos();
  }, []);

  return (
    <ul className='todo-list'>
      {reversedTodos.map((todo: ITodo) => (
        <TodosItem todo={todo} key={todo.id} />
      ))}
    </ul>
  );
}

export default TodosList;
