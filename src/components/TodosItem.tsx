import { KeyboardEvent, useContext, useRef, useState } from 'react';

// types
import { ITodo } from '../app/actionTypes';

import useActionCreators from '../hooks/useActionCreators';

interface IProps {
  todo: ITodo;
}

function TodoItem({ todo }: IProps) {
  const { id, title, completed } = todo;

  const [value, setValue] = useState<string>('');
  const [isEdit, setIsEdit] = useState<boolean>(false);
  const inputRef = useRef<HTMLInputElement | null>(null);

  const { deleteTodo, toggleCompletedTodo, editTodo } = useActionCreators();

  const handleToggleCompleted = () => {
    toggleCompletedTodo({
      id,
      title,
      completed: !completed,
    });
  };

  const handleToggleEdit = () => {
    setIsEdit(!isEdit);
  };

  const handleSubmitEdit = (e: KeyboardEvent<HTMLInputElement>) => {
    if (e.key === 'Enter') {
      editTodo({
        id,
        title: value,
        completed,
      });
      setIsEdit(false);
      setValue('');
    } else if (e.key === 'Escape') {
      setIsEdit(false);
      setValue('');
    }
  };

  return (
    <li className={completed ? 'completed' : isEdit ? 'editing' : ''}>
      <div className='view'>
        <input
          onChange={handleToggleCompleted}
          className='toggle'
          type='checkbox'
          checked={completed}
        />
        <label onClick={handleToggleEdit}>{title}</label>

        <button onClick={() => deleteTodo(id)} className='destroy'></button>
      </div>
      <input
        ref={inputRef}
        value={value}
        onChange={(e) => setValue(e.target.value)}
        onKeyDown={(e) => handleSubmitEdit(e)}
        className='edit'
        placeholder='Type something here...'
      />
    </li>
  );
}

export default TodoItem;
