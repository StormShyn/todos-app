import { KeyboardEvent, useState } from 'react';

// nanoid
import { nanoid } from 'nanoid';

import useActionCreators from '../hooks/useActionCreators';

function TodoHeader() {
  const [value, setValue] = useState<string>('');

  const { addTodo } = useActionCreators();

  const onHandleSubmit = (e: KeyboardEvent<HTMLInputElement>) => {
    if (e.key === 'Enter' && value.trim()) {
      addTodo({
        id: nanoid(),
        title: value,
        completed: false,
      });
      setValue('');
    }
  };

  return (
    <header className='header'>
      <h1>todos</h1>
      <input
        className='new-todo'
        onChange={(e) => setValue(e.target.value)}
        onKeyDown={onHandleSubmit}
        value={value}
        placeholder='What needs to be done?'
      />
    </header>
  );
}

export default TodoHeader;
