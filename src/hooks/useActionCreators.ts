import { useDispatch } from 'react-redux';
import { bindActionCreators } from 'redux';

import { actionCreators } from '../app/index';

const useActionCreators = () => {
  const dispatch = useDispatch();

  const {
    addTodo,
    getTodos,
    deleteTodo,
    toggleCompletedTodo,
    filterTodos,
    editTodo,
    toggleAllTodos,
  } = bindActionCreators(actionCreators, dispatch);

  return {
    addTodo,
    getTodos,
    deleteTodo,
    toggleCompletedTodo,
    filterTodos,
    editTodo,
    toggleAllTodos,
  };
};

export default useActionCreators;
