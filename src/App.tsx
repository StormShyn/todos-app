import TodosList from './components/TodosList';
import TodosHeader from './components/TodosHeader';
import TodosFooter from './components/TodosFooter';
import TodosToggle from './components/TodosToggle';

function App() {
  return (
    <section className='todo-app'>
      <TodosHeader />

      <section className='main'>
        <TodosToggle />
        <TodosList />
      </section>

      <TodosFooter />
    </section>
  );
}

export default App;
