// actions
import { ActionsEnum } from '../actions';

export interface ITodo {
  id: string;
  title: string;
  completed: boolean;
}

export type ITodosState = ITodo[];

interface IGetAction {
  type: ActionsEnum.GET_TODOS;
  payload: ITodosState;
}

interface IAddAction {
  type: ActionsEnum.ADD_TODO;
  payload: ITodo;
}

interface IToggleAction {
  type: ActionsEnum.TOGGLE_TODO;
  payload: string;
}

interface IToggleAllAction {
  type: ActionsEnum.TOGGLE_ALL_TODOS;
  payload: boolean;
}

interface IDeleteAction {
  type: ActionsEnum.DELETE_TODO;
  payload: string;
}

interface IFilterAction {
  type: ActionsEnum.FILTER_TODOS;
  payload: string;
}

interface IEditTodo {
  id: string;
  title: string;
}

interface IEditAction {
  type: ActionsEnum.EDIT_TODO;
  payload: IEditTodo;
}

export type ITodosAction =
  | IGetAction
  | IAddAction
  | IToggleAction
  | IToggleAllAction
  | IDeleteAction
  | IFilterAction
  | IEditAction;
