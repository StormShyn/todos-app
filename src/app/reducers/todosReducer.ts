// types
import { ITodosAction, ITodosState } from '../actionTypes';

// actions
import { ActionsEnum } from '../actions';

const initialState: ITodosState = [
  // data from db.json
];

const todosReducer = (
  state = initialState,
  { type, payload }: ITodosAction
) => {
  switch (type) {
    case ActionsEnum.GET_TODOS:
      return payload;
    case ActionsEnum.ADD_TODO:
      return [...state, payload];
    case ActionsEnum.TOGGLE_TODO:
      return state.map((todo) =>
        todo.id === payload ? { ...todo, completed: !todo.completed } : todo
      );
    case ActionsEnum.TOGGLE_ALL_TODOS:
      return state.map((todo) => {
        return { ...todo, completed: payload };
      });
    case ActionsEnum.DELETE_TODO:
      return state.filter((todo) => todo.id !== payload);
    case ActionsEnum.EDIT_TODO:
      return state.map((todo) =>
        todo.id === (payload as { id: string; title: string }).id
          ? {
              ...todo,
              title: (payload as { id: string; title: string })['title'],
            }
          : todo
      );
    default:
      return state;
  }
};

export default todosReducer;
