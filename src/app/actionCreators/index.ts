import { Dispatch } from 'redux';

// actions
import { ActionsEnum } from '../actions';

// axios
import axios from 'axios';

// types
import { ITodo, ITodosState, ITodosAction } from '../actionTypes';

import { PORT } from '../../constants/port';

export const getTodos = () => async (dispatch: Dispatch<ITodosAction>) => {
  try {
    const response = await axios.get(PORT);
    const data: ITodosState = response.data;

    dispatch({
      type: ActionsEnum.GET_TODOS,
      payload: data,
    });
  } catch (error) {
    console.error(error);
  }
};

export const addTodo =
  (todo: ITodo) => async (dispatch: Dispatch<ITodosAction>) => {
    try {
      await axios.post(PORT, todo);

      dispatch({
        type: ActionsEnum.ADD_TODO,
        payload: todo,
      });
    } catch (error) {
      console.error(error);
    }
  };

export const toggleCompletedTodo =
  (todo: ITodo) => async (dispatch: Dispatch<ITodosAction>) => {
    try {
      await axios.patch('http://localhost:4000/todos/' + todo.id, todo);

      dispatch({
        type: ActionsEnum.TOGGLE_TODO,
        payload: todo.id,
      });
    } catch (error) {
      console.log(error);
    }
  };

export const toggleAllTodos = (todo: ITodo) => async (dispatch: Dispatch) => {
  try {
    await axios.patch('http://localhost:4000/todos/' + todo.id, todo);

    dispatch({
      type: ActionsEnum.TOGGLE_ALL_TODOS,
      payload: todo.completed,
    });
  } catch (error) {
    console.log(error);
  }
};

export const deleteTodo =
  (id: string) => async (dispatch: Dispatch<ITodosAction>) => {
    try {
      await axios.delete('http://localhost:4000/todos/' + id);

      dispatch({
        type: ActionsEnum.DELETE_TODO,
        payload: id,
      });
    } catch (error) {
      console.log(error);
    }
  };

export const filterTodos =
  (status: string) => async (dispatch: Dispatch<ITodosAction>) => {
    try {
      const response = await axios.get(PORT, {
        params: { completed: status === 'active' ? false : true },
      });

      dispatch({
        type: ActionsEnum.GET_TODOS,
        payload: response.data,
      });
    } catch (error) {
      console.log(error);
    }
  };

export const editTodo =
  (todo: ITodo) => async (dispatch: Dispatch<ITodosAction>) => {
    try {
      await axios.patch('http://localhost:4000/todos/' + todo.id, todo);

      dispatch({
        type: ActionsEnum.EDIT_TODO,
        payload: todo,
      });
    } catch (error) {
      console.log(error);
    }
  };
